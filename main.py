import math


def Function(N):
    if N > 0:
        c = int(math.log10(N))
    else:
        print("Введите натуральное число: ")
        N = int(input())
        return Function(N)
    A = N // 10 ** c
    print(A)
    if N > A:
        N = N - A * 10 ** c
        return Function(N)


if __name__ == '__main__':
    N = int(input("Введите натуральное число: "))
    Function(N)
