import pytest

from main import Function


@pytest.mark.parametrize("N, expected", [(123, 123),
                                         (10, 10),
                                         (786578, 786578),
                                         (1234567890, 123456789)])
def test_task_on_correct_result(N, expected):
    assert Function(N) == expected

